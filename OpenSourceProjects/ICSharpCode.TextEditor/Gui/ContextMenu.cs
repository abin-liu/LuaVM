﻿using ICSharpCode.TextEditor.Actions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ICSharpCode.TextEditor {
    public partial class ContextMenu : ContextMenuStrip {
        TextAreaControl parent;
        public ContextMenu(TextAreaControl parent) {
            this.parent = parent;
            InitializeComponent();

            undo.Click += OnClickUndo;
            redo.Click += OnClickRedo;
            cut.Click += OnClickCut;
            copy.Click += OnClickCopy;
            paste.Click += OnClickPaste;
            delete.Click += OnClickDelete;
            selectAll.Click += OnSelectAll;
        }

        void OnClickCut(object sender, EventArgs e) {
            new Cut().Execute(parent.TextArea);
            parent.TextArea.Focus();
        }

        void OnClickUndo(object sender, EventArgs e) {
            parent.Undo();
            parent.TextArea.Focus();
        }

        void OnClickRedo(object sender, EventArgs e)
        {
            parent.Redo();
            parent.TextArea.Focus();
        }

        void OnClickCopy(object sender, EventArgs e) {
            new Copy().Execute(parent.TextArea);
            parent.TextArea.Focus();
        }

        void OnClickPaste(object sender, EventArgs e) {
            new Paste().Execute(parent.TextArea);
            parent.TextArea.Focus();
        }

        void OnClickDelete(object sender, EventArgs e)
        {
            new Delete().Execute(parent.TextArea);
            parent.TextArea.Focus();
        }

        void OnSelectAll(object sender, EventArgs e) {
            new SelectWholeDocument().Execute(parent.TextArea);
            parent.TextArea.Focus();
        }

        void OnOpening(object sender, CancelEventArgs e) {
            bool hasSelection = parent.SelectionManager.HasSomethingSelected;
            undo.Enabled = parent.Document.UndoStack.CanUndo;
            redo.Enabled = parent.Document.UndoStack.CanRedo;
            cut.Enabled = hasSelection;
            copy.Enabled = hasSelection;            
            paste.Enabled =  parent.TextArea.ClipboardHandler.EnablePaste;
            delete.Enabled = hasSelection;
            selectAll.Enabled = !string.IsNullOrEmpty(parent.Document.TextContent);

            undo.Text = GetMenuText("Undo");
            redo.Text = GetMenuText("Redo");
            cut.Text = GetMenuText("Cut");
            copy.Text = GetMenuText("Copy");
            paste.Text = GetMenuText("Paste");
            delete.Text = GetMenuText("Delete");
            selectAll.Text = GetMenuText("Select All");
        }

        string GetMenuText(string key)
        {
            if (string.IsNullOrEmpty(key) || parent.ContextMenuTexts == null)
			{
                return key;
			}

            string value = null;
            parent.ContextMenuTexts.TryGetValue(key, out value);
            return value ?? key;
        }
    }
}
