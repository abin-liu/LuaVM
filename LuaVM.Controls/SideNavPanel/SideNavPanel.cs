﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using MFGLib;

namespace LuaVM.Controls
{
	public partial class SideNavPanel : UserControl
	{
		public static readonly string CSFolder = Application.StartupPath + "\\CommonScripts";

		public delegate void ItemSelectDelegate(string filePath);

		public ItemSelectDelegate MRUSelect { get; set; }
		public ItemSelectDelegate CSSelect { get; set; }

		MRU m_mru = new MRU();
		CSHelper m_cs = new CSHelper();
		TreeNode m_nodeMRU = null;
		TreeNode m_nodeCS = null;

		/// <summary>
		/// 获取和设置控件的字体
		/// </summary>
		public override Font Font
		{
			get
			{
				return base.Font;
			}

			set
			{
				treeView1.Font = value;
				base.Font = value;
			}
		}

		public SideNavPanel()
		{
			InitializeComponent();

			m_mru.ItemAdd = MRU_OnAdd;
			m_mru.ItemRemove = MRU_OnRemove;

			m_nodeMRU = treeView1.Nodes["mru"];
			m_nodeCS = treeView1.Nodes["cs"];

			m_cs.Load(CSFolder);
			foreach (CSData data in m_cs.Scripts)
			{
				TreeNode node = new TreeNode();
				node.Text = data.Title;
				node.ToolTipText = data.Desc;
				node.Tag = data.Path;
				node.ImageIndex = 3;
				node.SelectedImageIndex = 3;
				m_nodeCS.Nodes.Add(node);
			}
		}

		public void InitializeMRU(string[] files)
		{
			m_mru.Initialize(files);
			m_nodeMRU.Nodes.Clear();

			foreach (string file in files)
			{
				if (!File.Exists(file))
				{
					continue;
				}

				TreeNode node = new TreeNode();
				node.Text = Path.GetFileName(file);
				node.ToolTipText = file;
				node.Tag = file;
				node.ImageIndex = 2;
				node.SelectedImageIndex = 2;
				m_nodeMRU.Nodes.Add(node);
			}
		}

		public string[] GetMRU()
		{
			return m_mru.ToArray();
		}

		public void AddMRU(string file)
		{
			m_mru.Add(file);
		}

		public void RemoveMRU(string file)
		{
			m_mru.Remove(file);
		}

		public void ExpandAll()
		{
			treeView1.ExpandAll();
		}

		private void MRU_OnAdd(int index, string file)
		{
			TreeNode node = new TreeNode();
			node.Text = Path.GetFileName(file);
			node.ToolTipText = file;
			node.Tag = file;
			node.ImageIndex = 2;
			node.SelectedImageIndex = 2;
			m_nodeMRU.Nodes.Insert(index, node);
			treeView1.SelectedNode = node;
		}

		private void MRU_OnRemove(int index, string item)
		{		
			if (index >= 0 && index < m_nodeMRU.Nodes.Count)
			{
				m_nodeMRU.Nodes.RemoveAt(index);
			}			
		}

		private void treeView1_AfterCollapse(object sender, TreeViewEventArgs e)
		{
			e.Node.ImageIndex = 0;
			e.Node.SelectedImageIndex = 0;
		}

		private void treeView1_AfterExpand(object sender, TreeViewEventArgs e)
		{
			e.Node.ImageIndex = 1;
			e.Node.SelectedImageIndex = 1;
		}

		private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
		{
			treeView1.SelectedNode = e.Node;
		}

		private void treeView1_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
		{
			if (e.Node.Parent == null)
			{
				return;
			}

			switch ((string)e.Node.Parent.Tag)
			{
				case "MRU":
					MRUSelect?.Invoke(e.Node.Tag as string);
					break;

				case "CS":
					CSSelect?.Invoke(e.Node.Tag as string);
					break;

				default:
					break;
			}
		}

		private void menuMRURemove_Click(object sender, EventArgs e)
		{
			TreeNode node = treeView1.SelectedNode;
			if (node != null && node.Parent == m_nodeMRU)
			{
				RemoveMRU(node.Tag as string);
			}
		}

		private void contextMenuMRU_Opening(object sender, CancelEventArgs e)
		{
			TreeNode node = treeView1.SelectedNode;
			if (node == null)
			{
				e.Cancel = true;
				return;
			}

			if (node.Parent == m_nodeMRU)
			{
				menuMRURemove.Visible = true;
				menuCSOpenFolder.Visible = false;
			}
			else if (node == m_nodeCS)
			{
				menuMRURemove.Visible = false;
				menuCSOpenFolder.Visible = true;
			}
			else
			{
				e.Cancel = true;
			}
		}

		private void menuCSOpenFolder_Click(object sender, EventArgs e)
		{
			System.Diagnostics.Process.Start("explorer.exe", CSFolder);
		}
	}
}
