﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace MFGLib
{
	/// <summary>
	/// WinForm菜单功能：MRU
	/// </summary>
	public sealed class MRU : IEnumerable
	{
		/// <summary>
		/// 元素集合
		/// </summary>
		List<string> m_files = new List<string>();

		/// <summary>
		/// 当MRU元素被添加或删除时调用
		/// </summary>
		/// <param name="index">元素下标</param>
		/// <param name="item">元素内容</param>
		public delegate void ItemChangeDelegate(int index, string item);

		/// <summary>
		/// MRU元素被添加
		/// </summary>
		public ItemChangeDelegate ItemAdd { get; set; }

		/// <summary>
		/// MRU元素被删除
		/// </summary>
		public ItemChangeDelegate ItemRemove { get; set; }

		/// <summary>
		/// 当前个数
		/// </summary>
		public int Count => m_files.Count;

		/// <summary>
		/// 最大允许个数
		/// </summary>
		public int MaxCount { get; set; } = 10;

		/// <summary>
		/// 获取指定项文件路径
		/// </summary>
		/// <param name="index">下表</param>
		/// <returns>指定项文件路径</returns>
		public string this[int index] => m_files[index];

		/// <summary>
		/// 用数组初始化
		/// </summary>
		/// <param name="items">初始文件列表</param>
		public void Initialize(string[] files)
		{
			m_files.Clear();
			if (files != null && files.Length > 0)
			{
				m_files.AddRange(files);
			}			
		}		

		/// <summary>
		/// 从完整路径获取显示文本，默认为文件名称
		/// </summary>
		/// <param name="item">完整路径</param>
		/// <returns>显示文本</returns>
		public static string DisplayItem(string item)
		{
			if (string.IsNullOrEmpty(item))
			{
				return "";
			}

			return System.IO.Path.GetFileName(item);
		}

		/// <summary>
		/// 添加新文件
		/// </summary>
		/// <param name="item">文件路径</param>
		public void Add(string item)
		{
			if (string.IsNullOrEmpty(item))
			{
				return;
			}

			int index = IndexOf(item);
			if (index == 0)
			{
				return;
			}

			if (index != -1)
			{				
				m_files.RemoveAt(index);
				ItemRemove?.Invoke(index, item);
			}
			
			m_files.Insert(0, item);
			ItemAdd?.Invoke(0, item);

			while (MaxCount > 0 && Count > MaxCount)
			{
				int last = Count - 1;
				string text = m_files[last];				
				m_files.RemoveAt(last);
				ItemRemove?.Invoke(last, text);
			}
		}
		
		/// <summary>
		/// 删除一个路径，通常用于文件打开失败后
		/// </summary>
		/// <param name="item">文件路径</param>
		public void Remove(string item)
		{
			if (string.IsNullOrEmpty(item))
			{
				return;
			}

			int index = IndexOf(item);
			if (index == -1)
			{
				return;
			}
			
			m_files.RemoveAt(index);
			ItemRemove?.Invoke(index, item);
		}		
		
		/// <summary>
		/// 查找特定文件
		/// </summary>
		/// <param name="item">文件路径</param>
		/// <returns>下标</returns>
		public int IndexOf(string item)
		{
			if (string.IsNullOrEmpty(item))
			{
				return -1;
			}

			return m_files.FindIndex(x => string.Compare(x, item, true) == 0);			
		}

		/// <summary>
		/// 转化为字符串数组形式
		/// </summary>
		/// <returns>字符串数组</returns>
		public string[] ToArray()
		{			
			return m_files.ToArray();
		}		

		/// <summary>
		/// foreach相关
		/// </summary>
		/// <returns></returns>
		public IEnumerator GetEnumerator()
		{
			return m_files.GetEnumerator();
		}
	}	
}
