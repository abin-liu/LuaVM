﻿namespace LuaVM.Controls
{
	partial class SideNavPanel
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("最近访问", 0, 0);
			System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("通用脚本", 0, 0);
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SideNavPanel));
			this.treeView1 = new System.Windows.Forms.TreeView();
			this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.menuMRURemove = new System.Windows.Forms.ToolStripMenuItem();
			this.menuCSOpenFolder = new System.Windows.Forms.ToolStripMenuItem();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.contextMenuStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// treeView1
			// 
			this.treeView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.treeView1.ContextMenuStrip = this.contextMenuStrip1;
			this.treeView1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.treeView1.HideSelection = false;
			this.treeView1.ImageIndex = 0;
			this.treeView1.ImageList = this.imageList1;
			this.treeView1.ItemHeight = 20;
			this.treeView1.Location = new System.Drawing.Point(0, 0);
			this.treeView1.Name = "treeView1";
			treeNode1.ImageIndex = 0;
			treeNode1.Name = "mru";
			treeNode1.SelectedImageIndex = 0;
			treeNode1.Tag = "MRU";
			treeNode1.Text = "最近访问";
			treeNode2.ImageIndex = 0;
			treeNode2.Name = "cs";
			treeNode2.SelectedImageIndex = 0;
			treeNode2.Tag = "CS";
			treeNode2.Text = "通用脚本";
			this.treeView1.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2});
			this.treeView1.SelectedImageIndex = 0;
			this.treeView1.ShowNodeToolTips = true;
			this.treeView1.Size = new System.Drawing.Size(157, 229);
			this.treeView1.TabIndex = 0;
			this.treeView1.AfterCollapse += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterCollapse);
			this.treeView1.AfterExpand += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterExpand);
			this.treeView1.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView1_NodeMouseClick);
			this.treeView1.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView1_NodeMouseDoubleClick);
			// 
			// contextMenuStrip1
			// 
			this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuMRURemove,
            this.menuCSOpenFolder});
			this.contextMenuStrip1.Name = "contextMenuMRU";
			this.contextMenuStrip1.Size = new System.Drawing.Size(181, 70);
			this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuMRU_Opening);
			// 
			// menuMRURemove
			// 
			this.menuMRURemove.Image = global::LuaVM.Controls.Properties.Resources.Remove;
			this.menuMRURemove.Name = "menuMRURemove";
			this.menuMRURemove.Size = new System.Drawing.Size(180, 22);
			this.menuMRURemove.Text = "从列表中移除";
			this.menuMRURemove.Click += new System.EventHandler(this.menuMRURemove_Click);
			// 
			// menuCSOpenFolder
			// 
			this.menuCSOpenFolder.Image = global::LuaVM.Controls.Properties.Resources.FolderOpen;
			this.menuCSOpenFolder.Name = "menuCSOpenFolder";
			this.menuCSOpenFolder.Size = new System.Drawing.Size(180, 22);
			this.menuCSOpenFolder.Text = "打开目录位置";
			this.menuCSOpenFolder.Click += new System.EventHandler(this.menuCSOpenFolder_Click);
			// 
			// imageList1
			// 
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
			this.imageList1.Images.SetKeyName(0, "FolderClose.png");
			this.imageList1.Images.SetKeyName(1, "FolderOpen.png");
			this.imageList1.Images.SetKeyName(2, "MRUScripts.png");
			this.imageList1.Images.SetKeyName(3, "CommonScripts.png");
			// 
			// SideNavPanel
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.treeView1);
			this.Name = "SideNavPanel";
			this.Size = new System.Drawing.Size(157, 229);
			this.contextMenuStrip1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TreeView treeView1;
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
		private System.Windows.Forms.ToolStripMenuItem menuMRURemove;
		private System.Windows.Forms.ToolStripMenuItem menuCSOpenFolder;
	}
}
