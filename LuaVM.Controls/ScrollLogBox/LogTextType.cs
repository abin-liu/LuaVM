﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace LuaVM.Controls
{
	/// <summary>
	/// RichLogBox中的文字类型
	/// </summary>
	public enum LogTextType
	{
		/// <summary>
		/// 正常（黑色）
		/// </summary>
		Normal = 0,

		/// <summary>
		/// 高亮（蓝色）
		/// </summary>
		Highlight,

		/// <summary>
		/// 错误（红色）
		/// </summary>
		Error,

		/// <summary>
		/// 关键（紫色）
		/// </summary>
		Critical,

		/// <summary>
		/// 警示（橙色）
		/// </summary>
		Warning,

		/// <summary>
		/// 冗余（灰色）
		/// </summary>
		Verbose,
	}

	static class LogTextHelper
	{
		/// <summary>
		/// 根据日志种类返回颜色
		/// </summary>
		/// <param name="type">日志种类</param>
		/// <returns>对应的颜色</returns>
		public static Color GetLogTypeColor(LogTextType type)
		{
			Color color;
			switch (type)
			{
				case LogTextType.Error:
					color = Color.Red;
					break;

				case LogTextType.Highlight:
					color = Color.Blue;
					break;

				case LogTextType.Critical:
					color = Color.Purple;
					break;

				case LogTextType.Warning:
					color = Color.Orange;
					break;

				case LogTextType.Verbose:
					color = Color.Gray;
					break;

				default:
					color = Color.Empty;
					break;
			}
			return color;
		}

	}
}
