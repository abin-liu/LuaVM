﻿namespace LuaVM.Controls
{
    partial class LuaEditBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.textEditorControl1 = new ICSharpCode.TextEditor.TextEditorControl();
			this.SuspendLayout();
			// 
			// textEditorControl1
			// 
			this.textEditorControl1.BackColor = System.Drawing.SystemColors.Window;
			this.textEditorControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textEditorControl1.Highlighting = null;
			this.textEditorControl1.Location = new System.Drawing.Point(0, 0);
			this.textEditorControl1.Name = "textEditorControl1";
			this.textEditorControl1.Padding = new System.Windows.Forms.Padding(6, 0, 0, 0);
			this.textEditorControl1.Size = new System.Drawing.Size(179, 69);
			this.textEditorControl1.TabIndex = 0;
			// 
			// LuaEditBox
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.textEditorControl1);
			this.Name = "LuaEditBox";
			this.Size = new System.Drawing.Size(179, 69);
			this.ResumeLayout(false);

        }

		#endregion

		private ICSharpCode.TextEditor.TextEditorControl textEditorControl1;
	}
}
