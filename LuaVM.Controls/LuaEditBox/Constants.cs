﻿using System;

namespace LuaVM.Controls
{
	static class Constants
	{
		public static readonly string UserFolder = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\LuaVM";
		public static readonly string SyntaxFile = UserFolder + "\\Syntax.xshd";
		public const string SyntaxName = "lua";
		public static readonly string TempSaveFile = UserFolder + "\\TempSave.lua";
	}
}
