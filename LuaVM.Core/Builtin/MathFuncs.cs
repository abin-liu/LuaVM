﻿using System;

namespace LuaVM.Core.Builtin
{
	class MathFuncs
	{
		public MathFuncs(LuaEnv lua)
		{
			lua.RegisterFunction<MathFuncs>("Distance", "math.distance");
		}

		// 计算平面上两点间直线距离
		public static double Distance(int x1, int y1, int x2, int y2)
		{
			return Math.Sqrt(Math.Pow(x1 - x2, 2) + Math.Pow(y1 - y2, 2));
		}
	}
}
