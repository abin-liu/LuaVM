; 脚本由 Inno Setup 脚本向导 生成！
; 有关创建 Inno Setup 脚本文件的详细资料请查阅帮助文档！

#define MyAppName "LuaVM"
#define MyAppEngName "LuaVM"
#define MyAppExeName "LuaVM.exe"
#define MyAppRegPathKey "Path"
#define MyAppVersion GetFileVersion("bin\LuaVM.exe")
#define MyAppPublisher "Abin Software Studio"
#define MyAppURL ""

[Setup]
; 注: AppId的值为单独标识该应用程序。
; 不要为其他安装程序使用相同的AppId值。
; (生成新的GUID，点击 工具|在IDE中生成GUID。)
AppId={{F2B14706-CFF2-4A01-9526-9D15FE06E741}
AppMutex={{E1E30999-23D0-4828-8938-44D53F3449C2}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
AppCopyright=Copyright (C) 2021, {#MyAppPublisher}
;LicenseFile=EULA.rtf
PrivilegesRequired=admin

DefaultDirName={reg:HKLM\Software\{#MyAppPublisher}\{#MyAppEngName},{#MyAppRegPathKey}|{pf}\{#MyAppEngName}}
DefaultGroupName={#MyAppName}
OutputBaseFilename=LuaVM_Setup {#MyAppVersion}
Compression=lzma
SolidCompression=yes

[Languages]
Name: "chinesesimp"; MessagesFile: "compiler:Default.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
Source: "bin\{#MyAppExeName}"; DestDir: "{app}"; Flags: ignoreversion
Source: "bin\ICSharpCode.TextEditor.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "bin\KopiLua.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "bin\LuaInterface.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "bin\LuaVM.chm"; DestDir: "{app}"; Flags: ignoreversion
Source: "bin\LuaVM.Controls.LuaEditBox.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "bin\LuaVM.Controls.ScrollLogBox.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "bin\LuaVM.Controls.SideNavPanel.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "bin\LuaVM.Core.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "bin\ToolkitForms.dll"; DestDir: "{app}"; Flags: ignoreversion

Source: "bin\CommonScripts\*"; DestDir: "{app}\CommonScripts\"; Flags: ignoreversion recursesubdirs

Source: "bin\Plugins\KopiLua.dll"; DestDir: "{app}\Plugins"; Flags: ignoreversion
Source: "bin\Plugins\LuaInterface.dll"; DestDir: "{app}\Plugins"; Flags: ignoreversion
Source: "bin\Plugins\LuaVM.Core.dll"; DestDir: "{app}\Plugins"; Flags: ignoreversion
Source: "bin\Plugins\Plugin.DC.dll"; DestDir: "{app}\Plugins"; Flags: ignoreversion
Source: "bin\Plugins\Plugin.DC.chm"; DestDir: "{app}\Plugins"; Flags: ignoreversion
Source: "bin\Plugins\Plugin.Input.dll"; DestDir: "{app}\Plugins"; Flags: ignoreversion
Source: "bin\Plugins\Plugin.Input.chm"; DestDir: "{app}\Plugins"; Flags: ignoreversion
Source: "bin\Plugins\Plugin.Window.dll"; DestDir: "{app}\Plugins"; Flags: ignoreversion
Source: "bin\Plugins\Plugin.Window.chm"; DestDir: "{app}\Plugins"; Flags: ignoreversion

; 注意: 不要在任何共享系统文件上使用“Flags: ignoreversion”

[Icons]
Name: "{commondesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopiconName: "{commonprograms}\{#MyAppName}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
Name: "{commonprograms}\{#MyAppName}\开发指南"; Filename: "{app}\{#MyAppEngName}.chm"
Name: "{commonprograms}\{#MyAppName}\卸载"; Filename: "{uninstallexe}"


[Registry]
Root: HKCU; Subkey: "Software\{#MyAppPublisher}\{#MyAppEngName}"; Flags: uninsdeletekey
Root: HKCU; Subkey: "Software\{#MyAppPublisher}"; Flags: uninsdeletekeyifempty
Root: HKLM; Subkey: "Software\{#MyAppPublisher}\{#MyAppEngName}"; ValueType: string; ValueName: {#MyAppRegPathKey}; ValueData: "{app}"

[Code]
const MutexName = '{E1E30999-23D0-4828-8938-44D53F3449C2}';
function InitializeSetup: Boolean;	
	begin
		if CheckForMutexes(MutexName) then
			begin
				MsgBox('LuaVM is running, close it and run setup again.', mbError, MB_OK);
				Result := False;
			end
		else
			begin
				Result := True;
			end		
	end;
