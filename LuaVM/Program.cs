﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using MFGLib;

namespace LuaVM
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			FrameSingleton fs = new FrameSingleton("{E1E30999-23D0-4828-8938-44D53F3449C2}");
			if (!fs.Create("^LuaVM - .+"))
			{
				fs.ActivatePrevHwnd();
				return;
			}

			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new MainForm());
		}
	}	
}
