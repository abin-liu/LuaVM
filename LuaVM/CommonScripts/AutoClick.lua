﻿--[[
##Title: 鼠标点击器

##Desc: 自动定时发送鼠标左键点击事件，智能侦测用户正在进行的手动操作，并避免对其产生干扰。

功能特点

1： 按下快捷键启动脚本时，最前台窗口为预设目标窗口，鼠标位置（相对于目标窗口左上角）为预设点击位置
2： 启动后每3秒检测下列条件，如符合则向点击点发送一次鼠标左键事件
    (a) 预设目标窗口处于系统最前台
    (b) 预设点击位置与当前鼠标位置（相对于目标窗口左上角）之间的直线距离不超过100像素
3： 如果以上条件不符合，说明用户很可能正在手动操控电脑，脚本静默等候3秒后重新检测

Abin Liu
2021-6-03
--]]

-- 参数设置
local INTERVAL = 3000 -- 点击间隔(毫秒)
local MAX_OFFSET = 100 -- 鼠标离点击点距离超过此值则暂停点击

-- 插件对象
local window = plugins.Window
local input = plugins.Input

local function GetOffsetCursorPos(hwnd)
	local x, y = input.GetCursorPos() -- 绝对坐标
	local offsetX, offsetY = window.WindowToScreen(hwnd)  -- 窗体相对于屏幕的位置
	return x - offsetX, y - offsetY -- 把绝对坐标转化为相对坐标
end

function main()
	-- 获取点击窗体和点击位置
	local hwnd = window.GetForegroundWindow() -- 记录点击对象窗体，前台非此窗口则暂停点击
	local x, y = GetOffsetCursorPos(hwnd) -- 记录点击位置的相对坐标

	print("自动点击开始: target=["..window.GetWindowText(hwnd).."], x="..x..", y="..y)

	local status = nil

	while (true) do
		local ok = true
		if window.GetForegroundWindow() ~= hwnd then
			ok = false
			if status ~= "foreground" then
				status = "foreground"
				print("暂停点击 - 目标窗体不在最前台")	
			end			
		end
	
		if ok then		
			local x1, y1 = GetOffsetCursorPos(hwnd)	-- 获取当前鼠标相对坐标
			local offset =  math.distance(x, y, x1, y1)
			if offset > MAX_OFFSET then
				ok = false
				if status ~= "cursor" then
					status = "cursor"
					print("暂停点击 - 鼠标位置远离点击点")
				end
			end	
		end
	
		if ok then
			if status ~= "ok" then
				status = "ok"
				print("正常点击")
			end

			local offsetX, offsetY = window.WindowToScreen(hwnd)  -- 窗体相对于屏幕的位置
			input.SetCursorPos(x + offsetX, y + offsetY)
			input.MouseClick()		
		end
	
		sleep(INTERVAL)
	end
end