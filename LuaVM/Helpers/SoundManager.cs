﻿using System;
using System.Media;
using LuaVM.Core;

namespace LuaVM.Helpers
{
	class SoundManager
	{		
		private SoundPlayer m_start = new SoundPlayer(Properties.Resources.ResourceManager.GetStream("SoundStart"));
		private SoundPlayer m_stop = new SoundPlayer(Properties.Resources.ResourceManager.GetStream("SoundStop"));
		private SoundPlayer m_alert = new SoundPlayer(Properties.Resources.ResourceManager.GetStream("SoundAlert"));
		private bool m_alerting = false;
		private DateTime m_startTime;

		public void Create(LuaEnv lua)
		{
			lua.RegisterFunction(this, "StartAlert");
			lua.RegisterFunction(this, "StopAlert");
			lua.RegisterFunction(this, "IsAlerting");
			lua.RegisterFunction(this, "AlertedFor");
		}

		public void PlayStart()
		{
			m_start.Play();
		}

		public void PlayStop()
		{
			m_stop.Play();
		}

		public void StartAlert()
		{
			m_alerting = true;
			m_startTime = DateTime.Now;
			m_alert.PlayLooping();			
		}

		public void StopAlert()
		{
			m_alerting = false;
			m_alert.Stop();			
		}

		public bool IsAlerting()
		{
			return m_alerting;
		}

		public int AlertedFor()
		{
			if (!m_alerting)
			{
				return 0;
			}

			return (int)(DateTime.Now - m_startTime).TotalMilliseconds;
		}
	}
}
