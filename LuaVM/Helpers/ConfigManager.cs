﻿using System.Windows.Forms;
using System.Drawing;
using System.Xml;
using ToolkitForms;
using MFGLib;

namespace LuaVM.Helpers
{
	class ConfigManager
	{
		const string XmlFileName = "Config.xml";

		public int Width { get; set; } = 1000;
		public int Height { get; set; } = 640;
		public bool Maximized { get; set; }
		public bool Modified { get; set; }
		public bool CloseToTray { get; set; }
		public int Split1Distance { get; set; } = 220;
		public int Split2Distance { get; set; } = 410;

		public string FileName { get; set; }
		public Font Font { get; set; } = new Font("Consolas", (float)9.75, FontStyle.Regular);		
		public HotkeyData Hotkey { get; set; } = new HotkeyData();
		public string[] MRU { get; set; } = new string[0];
		public string[] Plugins { get; set; } = new string[0];

		public void Load(string userFolder)
		{			
			XmlDocument xml = new XmlDocument();
			try
			{
				xml.Load(userFolder + '\\' + XmlFileName);
			}
			catch
			{
				return;
			}

			XmlNode root = xml.SelectSingleNode("/Config");
			Width = XmlHelper.ReadNodeInt(root, "Width");
			Height = XmlHelper.ReadNodeInt(root, "Height");
			Maximized = XmlHelper.ReadNodeInt(root, "Maximized") != 0;
			Modified = XmlHelper.ReadNodeInt(root, "Modified") != 0;
			CloseToTray = XmlHelper.ReadNodeInt(root, "CloseToTray") != 0;
			Split1Distance = XmlHelper.ReadNodeInt(root, "Split1Distance");
			Split2Distance = XmlHelper.ReadNodeInt(root, "Split2Distance");
			FileName = XmlHelper.ReadNodeString(root, "FileName");
			if (FileName == "")
			{
				FileName = null;
			}

			XmlNode node = root["Font"];
			if (node != null)
			{
				string name = XmlHelper.ReadNodeString(node, "Family");
				float size = (float)XmlHelper.ReadNodeDouble(node, "Size");
				FontStyle style = (FontStyle)XmlHelper.ReadNodeInt(node, "Style");
				Font = new Font(name, size, style);
			}

			node = root["Hotkey"];
			if (node != null)
			{
				Hotkey.Key = (Keys)XmlHelper.ReadNodeInt(node, "Key");
				Hotkey.Modifiers = XmlHelper.ReadNodeInt(node, "Modifiers");
			}

			MRU = XmlHelper.ReadNodeStringArray(root["MRU"], "File");
			Plugins = XmlHelper.ReadNodeStringArray(root["Plugins"], "File");
		}

		public void Save(string userFolder)
		{
			XmlDocument xml = new XmlDocument();
			XmlNode head = xml.CreateXmlDeclaration("1.0", "utf-8", "");
			xml.AppendChild(head);

			XmlNode root = xml.CreateElement("Config");
			xml.AppendChild(root);

			XmlNode node;

			node = xml.CreateElement("Width");
			node.InnerText = Width.ToString();
			root.AppendChild(node);

			node = xml.CreateElement("Height");
			node.InnerText = Height.ToString();
			root.AppendChild(node);

			node = xml.CreateElement("Maximized");
			node.InnerText = Maximized ? "1" : "0";
			root.AppendChild(node);

			node = xml.CreateElement("Modified");
			node.InnerText = Modified ? "1" : "0";
			root.AppendChild(node);

			node = xml.CreateElement("CloseToTray");
			node.InnerText = CloseToTray ? "1" : "0";
			root.AppendChild(node);

			node = xml.CreateElement("Split1Distance");
			node.InnerText = Split1Distance.ToString();
			root.AppendChild(node);

			node = xml.CreateElement("Split2Distance");
			node.InnerText = Split2Distance.ToString();
			root.AppendChild(node);

			node = xml.CreateElement("FileName");
			node.InnerText = FileName;
			root.AppendChild(node);

			XmlNode font = xml.CreateElement("Font");
			root.AppendChild(font);

			node = xml.CreateElement("Family");
			node.InnerText = Font.Name;
			font.AppendChild(node);

			node = xml.CreateElement("Size");
			node.InnerText = Font.Size.ToString();
			font.AppendChild(node);

			node = xml.CreateElement("Style");
			node.InnerText = ((int)Font.Style).ToString();
			font.AppendChild(node);

			XmlNode hotkey = xml.CreateElement("Hotkey");
			root.AppendChild(hotkey);

			node = xml.CreateElement("Modifiers");
			node.InnerText = Hotkey.Modifiers.ToString();
			hotkey.AppendChild(node);

			node = xml.CreateElement("Key");
			node.InnerText = ((int)Hotkey.Key).ToString();
			hotkey.AppendChild(node);

			XmlNode mru = xml.CreateElement("MRU");
			root.AppendChild(mru);

			foreach (string file in MRU)
			{
				node = xml.CreateElement("File");
				node.InnerText = file;
				mru.AppendChild(node);
			}

			XmlNode plugins = xml.CreateElement("Plugins");
			root.AppendChild(plugins);

			foreach (string file in Plugins)
			{
				node = xml.CreateElement("File");
				node.InnerText = file;
				plugins.AppendChild(node);
			}

			xml.Save(userFolder + '\\' + XmlFileName);
		}	
	}
}
