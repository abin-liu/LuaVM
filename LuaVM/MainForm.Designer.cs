﻿
namespace LuaVM
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.menuMain = new System.Windows.Forms.MenuStrip();
			this.menuFile = new System.Windows.Forms.ToolStripMenuItem();
			this.menuFileNew = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.menuFileOpen = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.menuFileSave = new System.Windows.Forms.ToolStripMenuItem();
			this.menuFileSaveAs = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
			this.menuFileExis = new System.Windows.Forms.ToolStripMenuItem();
			this.menuEdit = new System.Windows.Forms.ToolStripMenuItem();
			this.menuEditUndo = new System.Windows.Forms.ToolStripMenuItem();
			this.menuEditRedo = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
			this.menuEditCut = new System.Windows.Forms.ToolStripMenuItem();
			this.menuEditCopy = new System.Windows.Forms.ToolStripMenuItem();
			this.menuEditPaste = new System.Windows.Forms.ToolStripMenuItem();
			this.menuEditDelete = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
			this.menuEditSelectAll = new System.Windows.Forms.ToolStripMenuItem();
			this.actionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.menuActionRun = new System.Windows.Forms.ToolStripMenuItem();
			this.menuActionStop = new System.Windows.Forms.ToolStripMenuItem();
			this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.menuOptionsGlobalHotkey = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
			this.menuOptionsFont = new System.Windows.Forms.ToolStripMenuItem();
			this.menuOptionsCloseToTray = new System.Windows.Forms.ToolStripMenuItem();
			this.menuPlugins = new System.Windows.Forms.ToolStripMenuItem();
			this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.menuHelpLua51 = new System.Windows.Forms.ToolStripMenuItem();
			this.menuHelpLuaNMHelp = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
			this.menuHelpPluginManuals = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
			this.menuHelpAbout = new System.Windows.Forms.ToolStripMenuItem();
			this.toolbarMain = new System.Windows.Forms.ToolStrip();
			this.toolNew = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
			this.toolOpen = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
			this.toolSave = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
			this.toolUndo = new System.Windows.Forms.ToolStripButton();
			this.toolRedo = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
			this.toolRun = new System.Windows.Forms.ToolStripButton();
			this.toolStop = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
			this.toolHotkey = new System.Windows.Forms.ToolStripButton();
			this.trayIconMain = new System.Windows.Forms.NotifyIcon(this.components);
			this.contextMenuTray = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.menuPopupShowUI = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
			this.menuPopupRun = new System.Windows.Forms.ToolStripMenuItem();
			this.menuPopupStop = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
			this.menuPopupExit = new System.Windows.Forms.ToolStripMenuItem();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.sideNavPanel1 = new LuaVM.Controls.SideNavPanel();
			this.splitContainer2 = new System.Windows.Forms.SplitContainer();
			this.luaEditBox1 = new LuaVM.Controls.LuaEditBox();
			this.scrollLogBox1 = new LuaVM.Controls.ScrollLogBox();
			this.treeImageList = new System.Windows.Forms.ImageList(this.components);
			this.menuMain.SuspendLayout();
			this.toolbarMain.SuspendLayout();
			this.contextMenuTray.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
			this.splitContainer2.Panel1.SuspendLayout();
			this.splitContainer2.Panel2.SuspendLayout();
			this.splitContainer2.SuspendLayout();
			this.SuspendLayout();
			// 
			// menuMain
			// 
			this.menuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFile,
            this.menuEdit,
            this.actionToolStripMenuItem,
            this.optionsToolStripMenuItem,
            this.menuPlugins,
            this.helpToolStripMenuItem});
			this.menuMain.Location = new System.Drawing.Point(4, 4);
			this.menuMain.Name = "menuMain";
			this.menuMain.Size = new System.Drawing.Size(896, 25);
			this.menuMain.TabIndex = 0;
			this.menuMain.Text = "menuStrip1";
			// 
			// menuFile
			// 
			this.menuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFileNew,
            this.toolStripSeparator1,
            this.menuFileOpen,
            this.toolStripSeparator2,
            this.menuFileSave,
            this.menuFileSaveAs,
            this.toolStripSeparator3,
            this.menuFileExis});
			this.menuFile.Name = "menuFile";
			this.menuFile.Size = new System.Drawing.Size(58, 21);
			this.menuFile.Text = "文件(&F)";
			// 
			// menuFileNew
			// 
			this.menuFileNew.Image = global::LuaVM.Properties.Resources.New;
			this.menuFileNew.Name = "menuFileNew";
			this.menuFileNew.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
			this.menuFileNew.Size = new System.Drawing.Size(147, 22);
			this.menuFileNew.Text = "新建";
			this.menuFileNew.ToolTipText = "创建新脚本";
			this.menuFileNew.Click += new System.EventHandler(this.menuFileNew_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(144, 6);
			// 
			// menuFileOpen
			// 
			this.menuFileOpen.Image = global::LuaVM.Properties.Resources.Open;
			this.menuFileOpen.Name = "menuFileOpen";
			this.menuFileOpen.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
			this.menuFileOpen.Size = new System.Drawing.Size(147, 22);
			this.menuFileOpen.Text = "打开";
			this.menuFileOpen.ToolTipText = "打开一个现有脚本文件";
			this.menuFileOpen.Click += new System.EventHandler(this.menuFileOpen_Click);
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(144, 6);
			// 
			// menuFileSave
			// 
			this.menuFileSave.Image = global::LuaVM.Properties.Resources.Save;
			this.menuFileSave.Name = "menuFileSave";
			this.menuFileSave.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
			this.menuFileSave.Size = new System.Drawing.Size(147, 22);
			this.menuFileSave.Text = "保存";
			this.menuFileSave.ToolTipText = "保存当前脚本";
			this.menuFileSave.Click += new System.EventHandler(this.menuFileSave_Click);
			// 
			// menuFileSaveAs
			// 
			this.menuFileSaveAs.Image = global::LuaVM.Properties.Resources.SaveAs;
			this.menuFileSaveAs.Name = "menuFileSaveAs";
			this.menuFileSaveAs.Size = new System.Drawing.Size(147, 22);
			this.menuFileSaveAs.Text = "另存为";
			this.menuFileSaveAs.ToolTipText = "当前脚本另存为";
			this.menuFileSaveAs.Click += new System.EventHandler(this.menuFileSaveAs_Click);
			// 
			// toolStripSeparator3
			// 
			this.toolStripSeparator3.Name = "toolStripSeparator3";
			this.toolStripSeparator3.Size = new System.Drawing.Size(144, 6);
			// 
			// menuFileExis
			// 
			this.menuFileExis.Name = "menuFileExis";
			this.menuFileExis.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
			this.menuFileExis.Size = new System.Drawing.Size(147, 22);
			this.menuFileExis.Text = "退出";
			this.menuFileExis.ToolTipText = "退出程序";
			this.menuFileExis.Click += new System.EventHandler(this.menuFileExis_Click);
			// 
			// menuEdit
			// 
			this.menuEdit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuEditUndo,
            this.menuEditRedo,
            this.toolStripSeparator6,
            this.menuEditCut,
            this.menuEditCopy,
            this.menuEditPaste,
            this.menuEditDelete,
            this.toolStripSeparator5,
            this.menuEditSelectAll});
			this.menuEdit.Name = "menuEdit";
			this.menuEdit.Size = new System.Drawing.Size(59, 21);
			this.menuEdit.Text = "编辑(&E)";
			this.menuEdit.DropDownOpened += new System.EventHandler(this.menuEdit_DropDownOpened);
			// 
			// menuEditUndo
			// 
			this.menuEditUndo.Image = global::LuaVM.Properties.Resources.Undo;
			this.menuEditUndo.Name = "menuEditUndo";
			this.menuEditUndo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
			this.menuEditUndo.Size = new System.Drawing.Size(145, 22);
			this.menuEditUndo.Text = "撤销";
			this.menuEditUndo.Click += new System.EventHandler(this.menuEditUndo_Click);
			// 
			// menuEditRedo
			// 
			this.menuEditRedo.Image = global::LuaVM.Properties.Resources.Redo;
			this.menuEditRedo.Name = "menuEditRedo";
			this.menuEditRedo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
			this.menuEditRedo.Size = new System.Drawing.Size(145, 22);
			this.menuEditRedo.Text = "重做";
			this.menuEditRedo.Click += new System.EventHandler(this.menuEditRedo_Click);
			// 
			// toolStripSeparator6
			// 
			this.toolStripSeparator6.Name = "toolStripSeparator6";
			this.toolStripSeparator6.Size = new System.Drawing.Size(142, 6);
			// 
			// menuEditCut
			// 
			this.menuEditCut.Image = global::LuaVM.Properties.Resources.Cut;
			this.menuEditCut.Name = "menuEditCut";
			this.menuEditCut.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
			this.menuEditCut.Size = new System.Drawing.Size(145, 22);
			this.menuEditCut.Text = "剪切";
			this.menuEditCut.Click += new System.EventHandler(this.menuEditCut_Click);
			// 
			// menuEditCopy
			// 
			this.menuEditCopy.Image = global::LuaVM.Properties.Resources.Copy;
			this.menuEditCopy.Name = "menuEditCopy";
			this.menuEditCopy.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
			this.menuEditCopy.Size = new System.Drawing.Size(145, 22);
			this.menuEditCopy.Text = "复制";
			this.menuEditCopy.Click += new System.EventHandler(this.menuEditCopy_Click);
			// 
			// menuEditPaste
			// 
			this.menuEditPaste.Image = global::LuaVM.Properties.Resources.Paste;
			this.menuEditPaste.Name = "menuEditPaste";
			this.menuEditPaste.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
			this.menuEditPaste.Size = new System.Drawing.Size(145, 22);
			this.menuEditPaste.Text = "粘贴";
			this.menuEditPaste.Click += new System.EventHandler(this.menuEditPaste_Click);
			// 
			// menuEditDelete
			// 
			this.menuEditDelete.Image = global::LuaVM.Properties.Resources.Delete;
			this.menuEditDelete.Name = "menuEditDelete";
			this.menuEditDelete.ShortcutKeys = System.Windows.Forms.Keys.Delete;
			this.menuEditDelete.Size = new System.Drawing.Size(145, 22);
			this.menuEditDelete.Text = "删除";
			this.menuEditDelete.Click += new System.EventHandler(this.menuEditDelete_Click);
			// 
			// toolStripSeparator5
			// 
			this.toolStripSeparator5.Name = "toolStripSeparator5";
			this.toolStripSeparator5.Size = new System.Drawing.Size(142, 6);
			// 
			// menuEditSelectAll
			// 
			this.menuEditSelectAll.Name = "menuEditSelectAll";
			this.menuEditSelectAll.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
			this.menuEditSelectAll.Size = new System.Drawing.Size(145, 22);
			this.menuEditSelectAll.Text = "全选";
			this.menuEditSelectAll.Click += new System.EventHandler(this.menuEditSelectAll_Click);
			// 
			// actionToolStripMenuItem
			// 
			this.actionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuActionRun,
            this.menuActionStop});
			this.actionToolStripMenuItem.Name = "actionToolStripMenuItem";
			this.actionToolStripMenuItem.Size = new System.Drawing.Size(60, 21);
			this.actionToolStripMenuItem.Text = "操作(&A)";
			// 
			// menuActionRun
			// 
			this.menuActionRun.Image = global::LuaVM.Properties.Resources.Run;
			this.menuActionRun.Name = "menuActionRun";
			this.menuActionRun.Size = new System.Drawing.Size(100, 22);
			this.menuActionRun.Text = "运行";
			this.menuActionRun.ToolTipText = "运行当前脚本";
			this.menuActionRun.Click += new System.EventHandler(this.menuActionRun_Click);
			// 
			// menuActionStop
			// 
			this.menuActionStop.Enabled = false;
			this.menuActionStop.Image = global::LuaVM.Properties.Resources.Stop;
			this.menuActionStop.Name = "menuActionStop";
			this.menuActionStop.Size = new System.Drawing.Size(100, 22);
			this.menuActionStop.Text = "停止";
			this.menuActionStop.ToolTipText = "停止脚本运行";
			this.menuActionStop.Click += new System.EventHandler(this.menuActionStop_Click);
			// 
			// optionsToolStripMenuItem
			// 
			this.optionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuOptionsGlobalHotkey,
            this.toolStripSeparator15,
            this.menuOptionsFont,
            this.menuOptionsCloseToTray});
			this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
			this.optionsToolStripMenuItem.Size = new System.Drawing.Size(62, 21);
			this.optionsToolStripMenuItem.Text = "设置(&O)";
			// 
			// menuOptionsGlobalHotkey
			// 
			this.menuOptionsGlobalHotkey.Image = global::LuaVM.Properties.Resources.Keyboard;
			this.menuOptionsGlobalHotkey.Name = "menuOptionsGlobalHotkey";
			this.menuOptionsGlobalHotkey.Size = new System.Drawing.Size(148, 22);
			this.menuOptionsGlobalHotkey.Text = "全局热键";
			this.menuOptionsGlobalHotkey.ToolTipText = "设置脚本运行/停止的全局热键";
			this.menuOptionsGlobalHotkey.Click += new System.EventHandler(this.toolHotkey_Click);
			// 
			// toolStripSeparator15
			// 
			this.toolStripSeparator15.Name = "toolStripSeparator15";
			this.toolStripSeparator15.Size = new System.Drawing.Size(145, 6);
			// 
			// menuOptionsFont
			// 
			this.menuOptionsFont.Image = global::LuaVM.Properties.Resources.Font;
			this.menuOptionsFont.Name = "menuOptionsFont";
			this.menuOptionsFont.Size = new System.Drawing.Size(148, 22);
			this.menuOptionsFont.Text = "字体";
			this.menuOptionsFont.ToolTipText = "更改脚本编辑器字体";
			this.menuOptionsFont.Click += new System.EventHandler(this.menuOptionsFont_Click);
			// 
			// menuOptionsCloseToTray
			// 
			this.menuOptionsCloseToTray.Name = "menuOptionsCloseToTray";
			this.menuOptionsCloseToTray.Size = new System.Drawing.Size(148, 22);
			this.menuOptionsCloseToTray.Text = "关闭到通知栏";
			this.menuOptionsCloseToTray.ToolTipText = "关闭主窗口后程序最小化到通知栏而非退出";
			this.menuOptionsCloseToTray.Click += new System.EventHandler(this.menuOptionsCloseToTray_Click);
			// 
			// menuPlugins
			// 
			this.menuPlugins.Name = "menuPlugins";
			this.menuPlugins.Size = new System.Drawing.Size(59, 21);
			this.menuPlugins.Text = "插件(&P)";
			// 
			// helpToolStripMenuItem
			// 
			this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuHelpLua51,
            this.menuHelpLuaNMHelp,
            this.toolStripSeparator4,
            this.menuHelpPluginManuals,
            this.toolStripSeparator11,
            this.menuHelpAbout});
			this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
			this.helpToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.H)));
			this.helpToolStripMenuItem.Size = new System.Drawing.Size(61, 21);
			this.helpToolStripMenuItem.Text = "帮助(&H)";
			// 
			// menuHelpLua51
			// 
			this.menuHelpLua51.Image = global::LuaVM.Properties.Resources.Lua51;
			this.menuHelpLua51.Name = "menuHelpLua51";
			this.menuHelpLua51.Size = new System.Drawing.Size(165, 22);
			this.menuHelpLua51.Text = "Lua 5.1参考资料";
			this.menuHelpLua51.Click += new System.EventHandler(this.menuHelpLua51_Click);
			// 
			// menuHelpLuaNMHelp
			// 
			this.menuHelpLuaNMHelp.Name = "menuHelpLuaNMHelp";
			this.menuHelpLuaNMHelp.Size = new System.Drawing.Size(165, 22);
			this.menuHelpLuaNMHelp.Text = "LuaVM文档";
			this.menuHelpLuaNMHelp.Click += new System.EventHandler(this.menuHelpLuaNMHelp_Click);
			// 
			// toolStripSeparator4
			// 
			this.toolStripSeparator4.Name = "toolStripSeparator4";
			this.toolStripSeparator4.Size = new System.Drawing.Size(162, 6);
			// 
			// menuHelpPluginManuals
			// 
			this.menuHelpPluginManuals.Name = "menuHelpPluginManuals";
			this.menuHelpPluginManuals.Size = new System.Drawing.Size(165, 22);
			this.menuHelpPluginManuals.Text = "插件文档";
			// 
			// toolStripSeparator11
			// 
			this.toolStripSeparator11.Name = "toolStripSeparator11";
			this.toolStripSeparator11.Size = new System.Drawing.Size(162, 6);
			// 
			// menuHelpAbout
			// 
			this.menuHelpAbout.Name = "menuHelpAbout";
			this.menuHelpAbout.Size = new System.Drawing.Size(165, 22);
			this.menuHelpAbout.Text = "关于";
			this.menuHelpAbout.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
			// 
			// toolbarMain
			// 
			this.toolbarMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolNew,
            this.toolStripSeparator10,
            this.toolOpen,
            this.toolStripSeparator7,
            this.toolSave,
            this.toolStripSeparator8,
            this.toolUndo,
            this.toolRedo,
            this.toolStripSeparator9,
            this.toolRun,
            this.toolStop,
            this.toolStripSeparator14,
            this.toolHotkey});
			this.toolbarMain.Location = new System.Drawing.Point(4, 29);
			this.toolbarMain.Name = "toolbarMain";
			this.toolbarMain.Size = new System.Drawing.Size(896, 25);
			this.toolbarMain.TabIndex = 1;
			this.toolbarMain.Text = "toolStrip1";
			// 
			// toolNew
			// 
			this.toolNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolNew.Image = global::LuaVM.Properties.Resources.New;
			this.toolNew.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolNew.Name = "toolNew";
			this.toolNew.Size = new System.Drawing.Size(23, 22);
			this.toolNew.Text = "New";
			this.toolNew.ToolTipText = "创建新脚本";
			this.toolNew.Click += new System.EventHandler(this.toolNew_Click);
			// 
			// toolStripSeparator10
			// 
			this.toolStripSeparator10.Name = "toolStripSeparator10";
			this.toolStripSeparator10.Size = new System.Drawing.Size(6, 25);
			// 
			// toolOpen
			// 
			this.toolOpen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolOpen.Image = global::LuaVM.Properties.Resources.Open;
			this.toolOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolOpen.Name = "toolOpen";
			this.toolOpen.Size = new System.Drawing.Size(23, 22);
			this.toolOpen.Text = "Open";
			this.toolOpen.ToolTipText = "打开现有脚本文件";
			this.toolOpen.Click += new System.EventHandler(this.toolOpen_Click);
			// 
			// toolStripSeparator7
			// 
			this.toolStripSeparator7.Name = "toolStripSeparator7";
			this.toolStripSeparator7.Size = new System.Drawing.Size(6, 25);
			// 
			// toolSave
			// 
			this.toolSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolSave.Image = global::LuaVM.Properties.Resources.Save;
			this.toolSave.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolSave.Name = "toolSave";
			this.toolSave.Size = new System.Drawing.Size(23, 22);
			this.toolSave.Text = "Save";
			this.toolSave.ToolTipText = "保存当前脚本";
			this.toolSave.Click += new System.EventHandler(this.toolSave_Click);
			// 
			// toolStripSeparator8
			// 
			this.toolStripSeparator8.Name = "toolStripSeparator8";
			this.toolStripSeparator8.Size = new System.Drawing.Size(6, 25);
			// 
			// toolUndo
			// 
			this.toolUndo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolUndo.Image = global::LuaVM.Properties.Resources.Undo;
			this.toolUndo.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolUndo.Name = "toolUndo";
			this.toolUndo.Size = new System.Drawing.Size(23, 22);
			this.toolUndo.Text = "Undo";
			this.toolUndo.ToolTipText = "撤销上一步操作";
			this.toolUndo.Click += new System.EventHandler(this.toolUndo_Click);
			// 
			// toolRedo
			// 
			this.toolRedo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolRedo.Image = global::LuaVM.Properties.Resources.Redo;
			this.toolRedo.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolRedo.Name = "toolRedo";
			this.toolRedo.Size = new System.Drawing.Size(23, 22);
			this.toolRedo.Text = "Redo";
			this.toolRedo.ToolTipText = "重做被撤销的操作";
			this.toolRedo.Click += new System.EventHandler(this.toolRedo_Click);
			// 
			// toolStripSeparator9
			// 
			this.toolStripSeparator9.Name = "toolStripSeparator9";
			this.toolStripSeparator9.Size = new System.Drawing.Size(6, 25);
			// 
			// toolRun
			// 
			this.toolRun.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolRun.Image = global::LuaVM.Properties.Resources.Run;
			this.toolRun.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolRun.Name = "toolRun";
			this.toolRun.Size = new System.Drawing.Size(23, 22);
			this.toolRun.Text = "Run";
			this.toolRun.ToolTipText = "运行现有脚本";
			this.toolRun.Click += new System.EventHandler(this.toolRun_Click);
			// 
			// toolStop
			// 
			this.toolStop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolStop.Enabled = false;
			this.toolStop.Image = global::LuaVM.Properties.Resources.Stop;
			this.toolStop.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStop.Name = "toolStop";
			this.toolStop.Size = new System.Drawing.Size(23, 22);
			this.toolStop.Text = "Stop";
			this.toolStop.ToolTipText = "停止脚本运行";
			this.toolStop.Click += new System.EventHandler(this.toolStop_Click);
			// 
			// toolStripSeparator14
			// 
			this.toolStripSeparator14.Name = "toolStripSeparator14";
			this.toolStripSeparator14.Size = new System.Drawing.Size(6, 25);
			// 
			// toolHotkey
			// 
			this.toolHotkey.Image = global::LuaVM.Properties.Resources.Keyboard;
			this.toolHotkey.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.toolHotkey.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolHotkey.Name = "toolHotkey";
			this.toolHotkey.Size = new System.Drawing.Size(60, 22);
			this.toolHotkey.Text = "None";
			this.toolHotkey.ToolTipText = "设置脚本运行/停止的全局热键";
			this.toolHotkey.Click += new System.EventHandler(this.toolHotkey_Click);
			// 
			// trayIconMain
			// 
			this.trayIconMain.BalloonTipText = "LuaVM UI is hidden";
			this.trayIconMain.BalloonTipTitle = "LuaVM";
			this.trayIconMain.ContextMenuStrip = this.contextMenuTray;
			this.trayIconMain.Icon = ((System.Drawing.Icon)(resources.GetObject("trayIconMain.Icon")));
			this.trayIconMain.Text = "LuaVM";
			this.trayIconMain.DoubleClick += new System.EventHandler(this.menuPopupShowUI_Click);
			// 
			// contextMenuTray
			// 
			this.contextMenuTray.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuPopupShowUI,
            this.toolStripSeparator12,
            this.menuPopupRun,
            this.menuPopupStop,
            this.toolStripSeparator13,
            this.menuPopupExit});
			this.contextMenuTray.Name = "contextMenuStrip1";
			this.contextMenuTray.Size = new System.Drawing.Size(125, 104);
			// 
			// menuPopupShowUI
			// 
			this.menuPopupShowUI.Name = "menuPopupShowUI";
			this.menuPopupShowUI.Size = new System.Drawing.Size(124, 22);
			this.menuPopupShowUI.Text = "Show UI";
			this.menuPopupShowUI.Click += new System.EventHandler(this.menuPopupShowUI_Click);
			// 
			// toolStripSeparator12
			// 
			this.toolStripSeparator12.Name = "toolStripSeparator12";
			this.toolStripSeparator12.Size = new System.Drawing.Size(121, 6);
			// 
			// menuPopupRun
			// 
			this.menuPopupRun.Image = global::LuaVM.Properties.Resources.Run;
			this.menuPopupRun.Name = "menuPopupRun";
			this.menuPopupRun.Size = new System.Drawing.Size(124, 22);
			this.menuPopupRun.Text = "Run";
			this.menuPopupRun.Click += new System.EventHandler(this.toolRun_Click);
			// 
			// menuPopupStop
			// 
			this.menuPopupStop.Image = global::LuaVM.Properties.Resources.Stop;
			this.menuPopupStop.Name = "menuPopupStop";
			this.menuPopupStop.Size = new System.Drawing.Size(124, 22);
			this.menuPopupStop.Text = "Stop";
			this.menuPopupStop.Click += new System.EventHandler(this.toolStop_Click);
			// 
			// toolStripSeparator13
			// 
			this.toolStripSeparator13.Name = "toolStripSeparator13";
			this.toolStripSeparator13.Size = new System.Drawing.Size(121, 6);
			// 
			// menuPopupExit
			// 
			this.menuPopupExit.Name = "menuPopupExit";
			this.menuPopupExit.Size = new System.Drawing.Size(124, 22);
			this.menuPopupExit.Text = "Exit";
			this.menuPopupExit.Click += new System.EventHandler(this.menuFileExis_Click);
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.Location = new System.Drawing.Point(4, 54);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.sideNavPanel1);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
			this.splitContainer1.Size = new System.Drawing.Size(896, 355);
			this.splitContainer1.SplitterDistance = 214;
			this.splitContainer1.TabIndex = 2;
			// 
			// sideNavPanel1
			// 
			this.sideNavPanel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.sideNavPanel1.CSSelect = null;
			this.sideNavPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.sideNavPanel1.Location = new System.Drawing.Point(0, 0);
			this.sideNavPanel1.MRUSelect = null;
			this.sideNavPanel1.Name = "sideNavPanel1";
			this.sideNavPanel1.Size = new System.Drawing.Size(214, 355);
			this.sideNavPanel1.TabIndex = 0;
			// 
			// splitContainer2
			// 
			this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer2.Location = new System.Drawing.Point(0, 0);
			this.splitContainer2.Name = "splitContainer2";
			this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// splitContainer2.Panel1
			// 
			this.splitContainer2.Panel1.Controls.Add(this.luaEditBox1);
			// 
			// splitContainer2.Panel2
			// 
			this.splitContainer2.Panel2.Controls.Add(this.scrollLogBox1);
			this.splitContainer2.Size = new System.Drawing.Size(678, 355);
			this.splitContainer2.SplitterDistance = 239;
			this.splitContainer2.TabIndex = 0;
			// 
			// luaEditBox1
			// 
			this.luaEditBox1.BackColor = System.Drawing.SystemColors.Window;
			this.luaEditBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.luaEditBox1.ContextMenuTexts = ((System.Collections.Generic.Dictionary<string, string>)(resources.GetObject("luaEditBox1.ContextMenuTexts")));
			this.luaEditBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.luaEditBox1.FileName = null;
			this.luaEditBox1.Location = new System.Drawing.Point(0, 0);
			this.luaEditBox1.Modified = false;
			this.luaEditBox1.Name = "luaEditBox1";
			this.luaEditBox1.ReadOnly = false;
			this.luaEditBox1.Size = new System.Drawing.Size(678, 239);
			this.luaEditBox1.TabIndex = 0;
			// 
			// scrollLogBox1
			// 
			this.scrollLogBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.scrollLogBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.scrollLogBox1.Location = new System.Drawing.Point(0, 0);
			this.scrollLogBox1.MaxLength = 32767;
			this.scrollLogBox1.Name = "scrollLogBox1";
			this.scrollLogBox1.Size = new System.Drawing.Size(678, 112);
			this.scrollLogBox1.TabIndex = 0;
			// 
			// treeImageList
			// 
			this.treeImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("treeImageList.ImageStream")));
			this.treeImageList.TransparentColor = System.Drawing.Color.Transparent;
			this.treeImageList.Images.SetKeyName(0, "FolderClose.png");
			this.treeImageList.Images.SetKeyName(1, "FolderOpen.png");
			this.treeImageList.Images.SetKeyName(2, "MRUScripts.png");
			this.treeImageList.Images.SetKeyName(3, "CommonScripts.png");
			// 
			// MainForm
			// 
			this.AllowDrop = true;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(904, 413);
			this.Controls.Add(this.splitContainer1);
			this.Controls.Add(this.toolbarMain);
			this.Controls.Add(this.menuMain);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MainMenuStrip = this.menuMain;
			this.Name = "MainForm";
			this.Padding = new System.Windows.Forms.Padding(4);
			this.Text = "LuaVM";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
			this.Load += new System.EventHandler(this.MainForm_Load);
			this.ResizeEnd += new System.EventHandler(this.MainForm_ResizeEnd);
			this.DragDrop += new System.Windows.Forms.DragEventHandler(this.MainForm_DragDrop);
			this.DragEnter += new System.Windows.Forms.DragEventHandler(this.MainForm_DragEnter);
			this.menuMain.ResumeLayout(false);
			this.menuMain.PerformLayout();
			this.toolbarMain.ResumeLayout(false);
			this.toolbarMain.PerformLayout();
			this.contextMenuTray.ResumeLayout(false);
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.splitContainer2.Panel1.ResumeLayout(false);
			this.splitContainer2.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
			this.splitContainer2.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.MenuStrip menuMain;
		private System.Windows.Forms.ToolStripMenuItem menuFile;
		private System.Windows.Forms.ToolStripMenuItem menuFileNew;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripMenuItem menuFileOpen;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.ToolStripMenuItem menuFileSave;
		private System.Windows.Forms.ToolStripMenuItem menuFileSaveAs;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
		private System.Windows.Forms.ToolStripMenuItem menuFileExis;
		private System.Windows.Forms.ToolStripMenuItem menuEdit;
		private System.Windows.Forms.ToolStripMenuItem menuEditUndo;
		private System.Windows.Forms.ToolStripMenuItem menuEditRedo;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
		private System.Windows.Forms.ToolStripMenuItem menuEditCut;
		private System.Windows.Forms.ToolStripMenuItem menuEditCopy;
		private System.Windows.Forms.ToolStripMenuItem menuEditPaste;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
		private System.Windows.Forms.ToolStripMenuItem menuEditSelectAll;
		private System.Windows.Forms.ToolStripMenuItem actionToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem menuActionRun;
		private System.Windows.Forms.ToolStripMenuItem menuActionStop;
		private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem menuHelpAbout;
		private System.Windows.Forms.ToolStrip toolbarMain;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
		private System.Windows.Forms.ToolStripButton toolSave;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
		private System.Windows.Forms.ToolStripButton toolUndo;
		private System.Windows.Forms.ToolStripButton toolRedo;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
		private System.Windows.Forms.ToolStripButton toolRun;
		private System.Windows.Forms.ToolStripButton toolStop;
		private System.Windows.Forms.ToolStripMenuItem menuPlugins;
		private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem menuOptionsFont;
		private System.Windows.Forms.ToolStripMenuItem menuHelpLua51;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
		private System.Windows.Forms.ToolStripMenuItem menuOptionsGlobalHotkey;
		private System.Windows.Forms.ToolStripMenuItem menuHelpLuaNMHelp;
		private System.Windows.Forms.ToolStripMenuItem menuHelpPluginManuals;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
		private System.Windows.Forms.NotifyIcon trayIconMain;
		private System.Windows.Forms.ContextMenuStrip contextMenuTray;
		private System.Windows.Forms.ToolStripMenuItem menuPopupExit;
		private System.Windows.Forms.ToolStripMenuItem menuPopupShowUI;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
		private System.Windows.Forms.ToolStripMenuItem menuPopupRun;
		private System.Windows.Forms.ToolStripMenuItem menuPopupStop;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;
		private System.Windows.Forms.ToolStripMenuItem menuOptionsCloseToTray;
		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.SplitContainer splitContainer2;
		private System.Windows.Forms.ImageList treeImageList;
		private System.Windows.Forms.ToolStripButton toolOpen;
		private System.Windows.Forms.ToolStripButton toolNew;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
		private Controls.ScrollLogBox scrollLogBox1;
		private Controls.LuaEditBox luaEditBox1;
		private Controls.SideNavPanel sideNavPanel1;
		private System.Windows.Forms.ToolStripMenuItem menuEditDelete;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
		private System.Windows.Forms.ToolStripButton toolHotkey;
	}
}

